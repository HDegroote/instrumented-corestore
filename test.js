const test = require('brittle')
const promClient = require('prom-client')
const Corestore = require('corestore')
const RAM = require('random-access-memory')
const InstrumentedCorestore = require('./index')
const idEncoding = require('hypercore-id-encoding')

function setup (t, getName = null) {
  const store = new Corestore(RAM.reusable())

  const instrumentedStore = new InstrumentedCorestore(
    store, promClient, getName
  )

  // Otherwise prom-client attempts to register an existing metric
  t.teardown(() => promClient.register.clear())

  return { store, instrumentedStore }
}

test('Same core is not added multiple times', async t => {
  const { store, instrumentedStore } = setup(t)

  const cores = instrumentedStore.metrics.cores
  t.is(cores.size, 0, 'Sanity check')

  const name = 'core'
  {
    const core = store.get({ name })
    await core.ready()
    t.alike(
      [...cores.keys()],
      [idEncoding.normalize(core.key)],
      'Sanity check: correct core added'
    )
    await core.close()
    t.is(cores.size, 0, 'Closed core got deleted')
  }

  {
    const core = store.get({ name })
    await core.ready()
    t.alike(
      [...cores.keys()],
      [idEncoding.normalize(core.key)],
      'Core added back'
    )
  }
})

test('Default name is discovery key', async t => {
  const { store, instrumentedStore } = setup(t)

  const core = store.get({ name: 'core' })
  await core.ready()
  const discKey = idEncoding.normalize(core.discoveryKey)
  t.is(instrumentedStore.keyToName.get(discKey), discKey)
})

test('Can define own getName', async t => {
  const { store, instrumentedStore } = setup(t, () => 'dummy')

  const core = store.get({ name: 'core' })
  await core.ready()
  const discKey = idEncoding.normalize(core.discoveryKey)
  t.is(instrumentedStore.keyToName.get(discKey), 'dummy')
})
