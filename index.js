const idEncoding = require('hypercore-id-encoding')
const safetyCatch = require('safety-catch')
const detectType = require('hypercore-detector')
const Hyperbee = require('hyperbee')
const hypercrypto = require('hypercore-crypto')

const defaultGetName = (core) => idEncoding.normalize(core.discoveryKey)

module.exports = class InstrumentedCorestore {
  constructor (corestore, promClient, getName, opts) {
    this.keyToName = new Map()
    this.metrics = new Hypermetrics(promClient, this.keyToName, opts)
    this.getName = getName || defaultGetName

    corestore.on('core-open', async core => {
      this.metrics.add(core)
      this._updateNameInBg(core).catch(safetyCatch)
    })
    corestore.on('core-close', async core => {
      this.metrics.delete(core)
    })

    for (const core of [...corestore.cores.values()]) {
      this.metrics.add(core)
      this._updateNameInBg(core).catch(safetyCatch)
    }
  }

  async _updateNameInBg (core) {
    await core.ready()

    // We link the name with the discovery key to avoid including
    // the public key capability in the metrics
    const discKey = idEncoding.normalize(core.discoveryKey)
    if (this.keyToName.has(discKey)) return // To avoid complications at promClient level, we don't allow changing the name
    // If getName throws or returns null, it's not added
    // (for example, could be a secondary core of a hyperdrive))
    const name = await this.getName(core)
    if (name == null) return

    this.keyToName.set(discKey, name)

    if (await detectType(core, { wait: true }) === 'drive') { // DEVNOTE: Will hang until 1st block (header for bee) is available
      const bee = new Hyperbee(core)
      await bee.ready()

      const blobsKey = (await bee.getHeader()).metadata?.contentFeed
      if (!blobsKey) return // Looks like a hyperdrive without being one

      if (this.keyToName.has(blobsKey)) return // To avoid complications at promClient level, we don't allow changing the name
      const blobsDiscKey = idEncoding.normalize(hypercrypto.discoveryKey(blobsKey))
      this.keyToName.set(blobsDiscKey, `${name} (secondary core)`)
    }
  }
}

// Adapted from https://github.com/holepunchto/hypermetrics
class Hypermetrics {
  constructor (client, keyToName, opts = {}) {
    this.client = client
    this.detailed = !!opts.detailed

    this.cores = new Map()
    this._labelNames = ['discovery_key', 'name']
    this.keyToName = keyToName

    const self = this

    new this.client.Gauge({ // eslint-disable-line no-new
      name: 'hypercore_length',
      help: 'hypercore length',
      labelNames: this._labelNames,
      collect () {
        self._collectMetric((core) => core.length, this)
      }
    })

    new this.client.Gauge({ // eslint-disable-line no-new
      name: 'hypercore_indexed_length',
      help: 'hypercore indexed length',
      labelNames: this._labelNames,
      collect () {
        self._collectMetric((core) => core.indexedLength, this)
      }
    })

    new this.client.Gauge({ // eslint-disable-line no-new
      name: 'hypercore_contiguous_length',
      help: 'hypercore contiguous length',
      labelNames: this._labelNames,
      collect () {
        self._collectMetric((core) => core.contiguousLength, this)
      }
    })

    new this.client.Gauge({ // eslint-disable-line no-new
      name: 'hypercore_byte_length',
      help: 'hypercore byte length',
      labelNames: this._labelNames,
      collect () {
        self._collectMetric((core) => core.byteLength, this)
      }
    })

    new this.client.Gauge({ // eslint-disable-line no-new
      name: 'hypercore_contiguous_byte_length',
      help: 'hypercore contiguous byte length',
      labelNames: this._labelNames,
      collect () {
        self._collectMetric((core) => core.contiguousByteLength, this)
      }
    })

    new this.client.Gauge({ // eslint-disable-line no-new
      name: 'hypercore_fork',
      help: 'hypercore fork',
      labelNames: this._labelNames,
      collect () {
        self._collectMetric((core) => core.fork, this)
      }
    })

    new this.client.Gauge({ // eslint-disable-line no-new
      name: 'hypercore_nr_inflight_blocks',
      help: 'Total number of blocks in flight per core (summed across all peers)',
      labelNames: this._labelNames,
      collect () {
        self._collectMetric((core) => {
          return core.peers.reduce((total, peer) => total + peer.inflight, 0)
        }, this)
      }
    })

    new this.client.Gauge({ // eslint-disable-line no-new
      name: 'hypercore_max_inflight_blocks',
      help: 'Max number of blocks in flight per core (summed across all peers)',
      labelNames: this._labelNames,
      collect () {
        self._collectMetric((core) => {
          return core.peers.reduce((total, peer) => total + peer.getMaxInflight(), 0)
        }, this)
      }
    })

    new this.client.Gauge({ // eslint-disable-line no-new
      name: 'hypercore_peers',
      help: 'hypercore number of peers',
      labelNames: this._labelNames,
      collect () {
        self._collectMetric((core) => core.peers.length, this)
      }
    })

    this.uploadedBlocks = new this.client.Counter({
      name: 'hypercore_uploaded_blocks',
      help: 'hypercore uploaded blocks',
      labelNames: this._labelNames
    })

    this.uploadedBytes = new this.client.Counter({
      name: 'hypercore_uploaded_bytes',
      help: 'Nr of bytes uploaded, per core',
      labelNames: this._labelNames
    })

    this.downloadedBlocks = new this.client.Counter({
      name: 'hypercore_downloaded_blocks',
      help: 'hypercore downloaded blocks',
      labelNames: this._labelNames
    })

    this.downloadedBytes = new this.client.Counter({
      name: 'hypercore_downloaded_bytes',
      help: 'Nr of bytes downloaded, per core',
      labelNames: this._labelNames
    })

    this.downloadedBytesPerPeer = this.detailed
      ? new this.client.Counter({
        name: 'hypercore_peer_downloaded_bytes',
        help: 'Nr of bytes downloaded across all cores, per peer',
        labelNames: ['peer_key']
      })
      : null

    this.uploadedBytesPerPeer = this.detailed
      ? new this.client.Counter({
        name: 'hypercore_peer_uploaded_bytes',
        help: 'Nr of bytes uploaded across all cores, per peer',
        labelNames: ['peer_key']
      })
      : null
  }

  add (core, opts = {}) {
    const discKey = idEncoding.encode(core.discoveryKey)
    this.cores.set(idEncoding.normalize(core.key), core)

    core.on('upload', (startIndex, byteLength, from) => {
      const name = this.keyToName.get(discKey)
      if (!name) return

      this.uploadedBlocks.labels({ discovery_key: discKey, name }).inc()
      this.uploadedBytes.labels({ discovery_key: discKey, name }).inc(byteLength)

      if (this.uploadedBytesPerPeer) {
        const peerKey = idEncoding.encode(from.remotePublicKey)
        this.uploadedBytesPerPeer.labels({ peer_key: peerKey }).inc(byteLength)
      }
    })

    core.on('download', (startIndex, byteLength, from) => {
      const name = this.keyToName.get(discKey)
      if (!name) return

      this.downloadedBlocks.labels({ discovery_key: discKey, name }).inc()
      this.downloadedBytes.labels({ discovery_key: discKey, name }).inc(byteLength)

      if (this.downloadedBytesPerPeer) {
        const peerKey = idEncoding.encode(from.remotePublicKey)
        this.downloadedBytesPerPeer.labels({ peer_key: peerKey }).inc(byteLength)
      }
    })
  }

  delete (core) {
    this.cores.delete(idEncoding.normalize(core.key))
  }

  _collectMetric (getValue, metric) {
    for (const core of this.cores.values()) {
      const discKey = idEncoding.encode(core.discoveryKey)
      const name = this.keyToName.get(discKey)
      if (!name) continue
      metric.labels({ discovery_key: discKey, name }).set(getValue(core))
    }
  }

  get register () {
    return this.client.register
  }

  async getMetricsAsJSON () {
    return this.client.register.getMetricsAsJSON()
  }
}
